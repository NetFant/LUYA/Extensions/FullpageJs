<?php
namespace netfant\fullpageJs\tests;

use netfant\fullpageJs\tests\BaseFullpageJsBlockTestCase;
/**
 * Class FullpageSlideBlockTestCase
 *
 * Test case for the base FullpageJs Slide Block.
 *
 * @package netfant\fullpageJs\tests
 * @author Alexander Schmid <schmid@netfant.ch>
 * @since 1.0.0
 *
 * @todo Add admin tests
 */
class FullpageSlideBlockTest extends BaseFullpageJsBlockTestCase
{
    public $blockClass = 'netfant\fullpageJs\blocks\FullpageSlideBlock';

    /**
     * Test the block without vars, cfgs or placeholders
     */
    public function testBlockWithoutAnything()
    {
        $this->assertSame('<div class="slide"></div>', $this->renderFrontendNoSpace());
    }

    /**
     * Test the block with configs (cfgs)
     */
    public function testBlockWithConfig()
    {
        $this->block->setCfgValues([
            'cssClass' => ' container'
        ]);
        $this->assertSame('<div class="slide container"></div>', $this->renderFrontendNoSpace());
    }

    /**
     * Test the block with placeholders (placeholder)
     */
    public function testBlockWithPlaceholder()
    {
        $this->block->setPlaceholderValues([
            'content' => '<h1>The Title</h1>'
        ]);
        $this->assertSame('<div class="slide"><h1>The Title</h1></div>', $this->renderFrontendNoSpace());
    }
}