<?php
namespace netfant\fullpageJs\tests;

use luya\testsuite\cases\WebApplicationTestCase;
use luya\testsuite\traits\MessageFileCompareTrait;

class MessageFileTest extends WebApplicationTestCase
{
    use MessageFileCompareTrait;

    /**
     * Provide Configurtion Array.
     */
    public function getConfigArray()
    {
        return [
            'id' => 'fullpageJsMessageTest',
            'basePath' => dirname(__DIR__)
        ];
    }

    public function testMessages()
    {
        $this->assertTrue($this->compareMessages($this->getConfigArray()['basePath'].'/src/messages', 'en'));
    }
}