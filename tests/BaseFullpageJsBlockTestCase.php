<?php
namespace netfant\fullpageJs\tests;

use luya\testsuite\cases\CmsBlockTestCase;
/**
 * Class BaseFullpageJsBlockTestCase
 *
 * Test case for the base FullpageJs Block.
 *
 * @package netfant\fullpageJs\tests
 * @author Alexander Schmid <schmid@netfant.ch>
 * @since 1.0.0
 */
abstract class BaseFullpageJsBlockTestCase extends CmsBlockTestCase
{
    /**
     * Provide Configurtion Array.
     */
    public function getConfigArray()
    {
        return [
            'id' => 'fullpageJsBlockTest',
            'basePath' => dirname(__DIR__) . '/../'
        ];
    }
}