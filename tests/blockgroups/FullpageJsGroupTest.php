<?php
namespace netfant\fullpageJs\tests\blockgroups;

use luya\testsuite\cases\CmsBlockGroupTestCase;
use luya\testsuite\cases\WebApplicationTestCase;
use netfant\fullpageJs\blockgroups\FullpageJsGroup;

/**
 * Class FullpageJsGroupTest
 *
 * Test for the  FullpageJs blockgroup.
 *
 * @package netfant\fullpageJs\tests
 * @author Alexander Schmid <schmid@netfant.ch>
 * @since 1.0.0
 *
 * @todo Add admin tests
 */
class FullpageJsGroupTest extends CmsBlockGroupTestCase
{
    public $blockGroupClass = 'netfant\fullpageJs\blockgroups\FullpageJsGroup';

    public $blockGroupIdentifier = 'fullpageJs';
}