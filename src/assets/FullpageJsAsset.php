<?php
namespace netfant\fullpageJs\assets;

use luya\web\Asset;

/**
 * Class FullpageJsAsset
 *
 * The fullpage js asset is used to add the needed base css and js resources for Fullpage.js.
 *
 * The asset must be registered in view
 *
 * ```FullpageJsAsset::register($this);```
 *
 * The config can be adjusted to use e.g. extensions or load from CDN.
 *
 * ```'netfant\fullpageJs\FullpageJsAsset' => [
 *      'css' => [
 *          '//cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/fullpage.min.css'
 *      ],
 *      'js' => [
 *          '//cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/vendors/scrolloverflow.min.js',
 *          '//cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/fullpage.extensions.min.js'
 *      ],
 *      'depends' => [
 *          'app\assets\FullpageExtensionAsset',
 *          'yii\web\JqueryAsset'
 *      ]
 *  ],```
 *
 * @todo Correct this file to make it work
 * @package netfant\fullpageJs
 */
class FullpageJsAsset extends Asset
{
    public $sourcePath = '@fullpageJs/resources';

    public $css = [
        'node_modules/fullpage.js/dist/fullpage.css'
    ];

    public $js = [
        'node_modules/fullpage.js/dist/fullpage.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset'
    ];
}