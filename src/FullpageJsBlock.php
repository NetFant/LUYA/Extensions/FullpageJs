<?php
namespace netfant\fullpageJs;

use luya\cms\base\PhpBlock;
use netfant\fullpageJs\blockgroups\FullpageJsGroup;

/**
 * Class FullpageJsBlock
 *
 * Base block for Fullpage.Js blocks
 * Sets the default view path
 *
 * @package netfant\fullpageJs
 * @author Alexander Schmid <schmid@netfant.ch>
 * @since 1.0.0
 */
abstract class FullpageJsBlock extends PhpBlock
{
    /**
     * @inheritdoc
     */
    public function getViewPath()
    {
        return dirname(__DIR__) . '/src/views/blocks';
    }

    /**
     * @inheritDoc
     */
    public function blockGroup()
    {
        return FullpageJsGroup::class;
    }
}