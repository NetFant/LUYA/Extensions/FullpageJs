<?php
return [
    'block_group.group_name' => 'Fullpage.js',

    'block_section.block_name' => 'Sektion (Fullpage.js)',
    'block_section.cfg_css_class' => 'CSS Klasse',
    'block_section.placeholder_content_label' => 'Sektionsinhalt',
    'block_section.admin_block_label' => 'Sektion (Fullpage.js)',

    'block_slide.block_name' => 'Slide (Fullpage.js)',
    'block_slide.cfg_css_class' => 'CSS Klasse',
    'block_slide.placeholder_content_label' => 'Slideinhalt',
    'block_slide.admin_block_label' => 'Slide (Fullpage.js)',
];
?>