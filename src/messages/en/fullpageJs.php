<?php
return [
    'block_group.group_name' => 'Fullpage.js',

    'block_section.block_name' => 'Section (Fullpage.js)',
    'block_section.cfg_css_class' => 'CSS Class',
    'block_section.placeholder_content_label' => 'Section Content',
    'block_section.admin_block_label' => 'Section (Fullpage.js)',

    'block_slide.block_name' => 'Slide (Fullpage.js)',
    'block_slide.cfg_css_class' => 'CSS Class',
    'block_slide.placeholder_content_label' => 'Slide Content',
    'block_slide.admin_block_label' => 'Slide (Fullpage.js)',
];
?>