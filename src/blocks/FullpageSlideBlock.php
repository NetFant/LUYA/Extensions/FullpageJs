<?php

namespace netfant\fullpageJs\blocks;

use netfant\fullpageJs\FullpageJsBlock;
use netfant\fullpageJs\Module;

/**
 * Fullpage Slide Block.
 *
 * File has been created with `block/create` command. 
 */
class FullpageSlideBlock extends FullpageJsBlock
{
    /**
     * @var boolean Choose whether block is a layout/container/segmnet/section block or not, Container elements will be optically displayed
     * in a different way for a better user experience. Container block will not display isDirty colorizing.
     */
    public $isContainer = true;

    /**
     * @var bool Choose whether a block can be cached trough the caching component. Be carefull with caching container blocks.
     */
    public $cacheEnabled = true;
    
    /**
     * @var int The cache lifetime for this block in seconds (3600 = 1 hour), only affects when cacheEnabled is true
     */
    public $cacheExpiration = 3600;

    /**
     * @inheritDoc
     */
    public function name()
    {
        return Module::t('block_slide.block_name');
    }
    
    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'aspect_ratio';
    }
 
    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'cfgs' => [
                ['var' => 'cssClass', 'label' => Module::t('block_slide.cfg_css_class'), 'type' => self::TYPE_TEXT],
            ],
            'placeholders' => [
                 ['var' => 'content', 'label' => Module::t('block_slide.placeholder_content_label')],
            ],
        ];
    }
    
    /**
     * {@inheritDoc} 
     *
     * @param {{placeholders.content}}
    */
    public function admin()
    {
        return '<h5 class="mb-3">' . Module::t('block_slide.admin_block_label') . '</h5>' .
            '<table class="table table-bordered">' .
            '{{placeholders.content}}' .
            '</table>';
    }
}