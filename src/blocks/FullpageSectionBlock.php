<?php

namespace netfant\fullpageJs\blocks;

use netfant\fullpageJs\FullpageJsBlock;
use netfant\fullpageJs\Module;

/**
 * Fullpage Section Block.
 *
 * The section block helps to build a fullpage section.
 * This means, it is a single height unit. A section can have slides in it, which can go to the side.
 *
 * @package netfant\fullpageJs
 * @author Alexander Schmid <schmid@netfant.ch>
 * @since 1.0.0
 */
class FullpageSectionBlock extends FullpageJsBlock
{
    /**
     * @var boolean Choose whether block is a layout/container/segmnet/section block or not, Container elements will be optically displayed
     * in a different way for a better user experience. Container block will not display isDirty colorizing.
     */
    public $isContainer = true;

    /**
     * @var bool Choose whether a block can be cached trough the caching component. Be carefull with caching container blocks.
     */
    public $cacheEnabled = true;

    /**
     * @var int The cache lifetime for this block in seconds (3600 = 1 hour), only affects when cacheEnabled is true
     */
    public $cacheExpiration = 3600;

    /**
     * @inheritDoc
     */
    public function name()
    {
        return Module::t('block_section.block_name');
    }
    
    /**
     * @inheritDoc
     */
    public function icon()
    {
        return 'stay_current_landscape';
    }
 
    /**
     * @inheritDoc
     */
    public function config()
    {
        return [
            'cfgs' => [
                ['var' => 'cssClass', 'label' => Module::t('block_section.cfg_css_class'), 'type' => self::TYPE_TEXT],
            ],
            'placeholders' => [
                 ['var' => 'content', 'label' => Module::t('block_section.placeholder_content_label')],
            ],
        ];
    }
    
    /**
     * {@inheritDoc} 
     *
     * @param {{placeholders.content}}
    */
    public function admin()
    {
        return '<h5 class="mb-3">' . Module::t('block_section.admin_block_label') . '</h5>' .
            '<table class="table table-bordered">' .
            '{{placeholders.content}}' .
            '</table>';
    }
}