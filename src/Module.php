<?php
namespace netfant\fullpageJs;

/**
 * Class Module
 *
 * The module class helps to register translations.
 *
 * @package netfant\fullpageJs
 * @author Alexander Schmid <schmid@netfant.ch>
 * @since 1.0.0
 */
class Module extends \luya\base\Module
{
    /**
     * @inheritdoc
     */
    public static function onLoad()
    {
        self::registerTranslation('fullpageJs', self::staticBasePath() . '/messages', [
            'fileMap' => [
                'fullpageJs' => 'fullpageJs.php'
            ]
        ]);
    }

    /**
     * Translations
     *
     * @param string $message
     * @param array $params
     * @return string
     */
    public static function t($message, array $params = [])
    {
        return parent::baseT('fullpageJs', $message, $params);
    }
}