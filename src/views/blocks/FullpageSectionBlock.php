<?php
/**
 * View file for block: FullpageSectionBlock 
 *
 * File has been created with `block/create` command. 
 *
 * @param $this->placeholderValue('block');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */

// Todo: Needs menu
?>
<div class="section<?= $this->cfgValue('cssClass') ?>">
    <?= $this->placeholderValue('content') ?>
</div>