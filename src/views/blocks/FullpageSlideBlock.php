<?php
/**
 * View file for block: FullpageSlideBlock 
 *
 * File has been created with `block/create` command. 
 *
 * @param $this->placeholderValue('blockContent');
 *
 * @var $this \luya\cms\base\PhpBlockView
 */
?>
<div class="slide<?= $this->cfgValue('cssClass') ?>">
    <?= $this->placeholderValue('content') ?>
</div>