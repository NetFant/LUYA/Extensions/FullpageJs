<?php

namespace netfant\fullpageJs\blockgroups;

use luya\cms\base\BlockGroup;
use netfant\fullpageJs\Module;

class FullpageJsGroup extends BlockGroup
{
    public function identifier()
    {
        return 'fullpageJs';
    }

    public function label()
    {
        return Module::t('block_group.group_name');
    }
}