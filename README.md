# LUYA Fullpage.js Extension

[![NetFant](https://img.shields.io/badge/powered_by-netfant-orange.svg?longCache=true&style=flat-square)](https://www.netfant.ch)
[![Packagist](https://img.shields.io/packagist/dt/netfant/luya-fullpagejs.svg?longCache=true&style=flat-square)](https://gitlab.com/NetFant/LUYA/Extensions/FullpageJs/)
[![Packagist](https://img.shields.io/packagist/l/netfant/luya-fullpagejs.svg?longCache=true&style=flat-square)](https://gitlab.com/NetFant/LUYA/Extensions/FullpageJs/blob/master/LICENSE)
[![Packagist](https://img.shields.io/packagist/v/netfant/luya-fullpagejs.svg?longCache=true&style=flat-square)](https://packagist.org/packages/netfant/luya-fullpagejs)

This package includes [Fullpage.js](https://alvarotrigo.com/fullPage/) ready to use for LUYA CMS.
Fullpage.js helps to build fullscreen scrolling websites with ease.

Further information:

- [Fullpage.js website](https://alvarotrigo.com/fullPage/)
- [Fullpage.js on GitHub](https://github.com/alvarotrigo/fullpage.js)
- [Fullpage.js extensions](https://alvarotrigo.com/fullPage/extensions/)
- [LUYA-fullpageJs on GitLab](https://gitlab.com/NetFant/LUYA/Extensions/FullpageJs)


## Features

- Fullpage.js section block
- Fullpage.js slide block
- Fullpage.js asset


## Installation & Configuration

### Composer Installation

The package needs to be installed through composer:

```
composer require netfant/luya-fullpageJs:1.0.0
```

### Alias config

The alias must be added to the configuration

```
'aliases' => [
    '@fullpageJs' => '@app/vendor/netfant/luya-fullpagejs/src',
],
```

### Block Import

The blocks need to be imported

```
vendor/bin/luya import
```

### Layout Setup

The layouts need to adopted to work with fullPage.

It should be kept in mind that LUYA is working with the default Yii-Layouts as well as with its own LUYA cms layouts.
Therefore both of these must be adapted to work with Fullpage.js.

Fullpage.js needs a wrapper where sections are placed inside.

`<div id="fullpage"></div>`

#### Yii Layouts

The default Yii layout must be adapted to work with Fullpage.js. It can be found under:
- "views/layouts/main"

Either a new layout must be set and afterwards be taken inside the CMS page settings, or the main layout can be used directly.

#### LUYA CMS Layouts

The LUYA cms layout must be adapted. It can be found under:
- "views/cmslayouts/main"

Either a new layout must be set and afterwards be taken inside the CMS layout settings, or the main layout can be used directly.

### Include Assets and needed Javascript

The FullpageJsAsset needs to be included. This can be done by e.g. registering the asset inside the layout.
The probably better approach is using a new Asset which depends on FullpageJsAsset. 
Like this, the initialization depend on that asset.

### Initialization

The last step is initializing Fullpage.js.
This can be done by adding some more JavaScript to the page.
This is the full config which can be found on the [fullpage.js github repo](https://github.com/alvarotrigo/fullpage.js).

```
var myFullpage = new fullpage('#fullpage', {
	//Navigation
	menu: '#menu',
	lockAnchors: false,
	anchors:['firstPage', 'secondPage'],
	navigation: false,
	navigationPosition: 'right',
	navigationTooltips: ['firstSlide', 'secondSlide'],
	showActiveTooltip: false,
	slidesNavigation: false,
	slidesNavPosition: 'bottom',

	//Scrolling
	css3: true,
	scrollingSpeed: 700,
	autoScrolling: true,
	fitToSection: true,
	fitToSectionDelay: 1000,
	scrollBar: false,
	easing: 'easeInOutCubic',
	easingcss3: 'ease',
	loopBottom: false,
	loopTop: false,
	loopHorizontal: true,
	continuousVertical: false,
	continuousHorizontal: false,
	scrollHorizontally: false,
	interlockedSlides: false,
	dragAndMove: false,
	offsetSections: false,
	resetSliders: false,
	fadingEffect: false,
	normalScrollElements: '#element1, .element2',
	scrollOverflow: false,
	scrollOverflowReset: false,
	scrollOverflowOptions: null,
	touchSensitivity: 15,
	normalScrollElementTouchThreshold: 5,
	bigSectionsDestination: null,

	//Accessibility
	keyboardScrolling: true,
	animateAnchor: true,
	recordHistory: true,

	//Design
	controlArrows: true,
	verticalCentered: true,
	sectionsColor : ['#ccc', '#fff'],
	paddingTop: '3em',
	paddingBottom: '10px',
	fixedElements: '#header, .footer',
	responsiveWidth: 0,
	responsiveHeight: 0,
	responsiveSlides: false,
	parallax: false,
	parallaxOptions: {type: 'reveal', percentage: 62, property: 'translate'},

	//Custom selectors
	sectionSelector: '.section',
	slideSelector: '.slide',

	lazyLoading: true,

	//events
	onLeave: function(origin, destination, direction){},
	afterLoad: function(origin, destination, direction){},
	afterRender: function(){},
	afterResize: function(){width, height},
	afterResponsive: function(isResponsive){},
	afterSlideLoad: function(section, origin, destination, direction){},
	onSlideLeave: function(section, origin, destination, direction){}
});
```

## Fullpage.js extensions

Fullpage.js extensions can be used by including the javascript files before our asset and adjusting the fullpage.js configuration.

E.g. adding Drag And Move extension:

> A project asset has to be set up with the needed JS files

```
<?php
namespace app\assets;
/**
 * Fullpage Extensions Asset File.
 */
class FullpageExtensionAsset extends \luya\web\Asset
{
    public $sourcePath = '@app/resources/dist/'.YII_ENV;
    
    public $js = [
        'DragAndMove.js'
    ];
}
```

> The application config has to be adjusted that FullpageJsAsset depends on the newly created extension asset

```
'assetManager' => [
    'class' => 'luya\web\AssetManager',
    'bundles' => [
        'netfant\fullpageJs\FullpageJsAsset' => [
            'sourcePath' => null,
            'css' => [
                '//cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/fullpage.min.css'
            ],
            'js' => [
                '//cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/vendors/scrolloverflow.min.js',
                '//cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/fullpage.extensions.min.js'
            ],
            'depends' => [
                'app\assets\FullpageExtensionAsset',
            ]
        ],
    ],
],
```

- The Fullpage.js config has to be adjusted
`dragAndMove: true`

## License

Fullpage.js licensing information can be found [here](https://alvarotrigo.com/fullPage/pricing/#faqs).


## Bugs / Problems

If any problems are found or bugs are encountered an issue should be made on [GitLab](https://gitlab.com/NetFant/LUYA/Extensions/FullpageJs/issues), so that it can be solved.
Moreover pull requests are very welcome for encountered problems/errors.